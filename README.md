# Managed book list #

Install dependencies:
```
npm install
```

Run tests:
```
npm test
```

Build and run:
```
npm run build
npm start
```
