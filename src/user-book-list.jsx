import data from '../content/data';

export default class UserBookList {
  constructor(name) {
    this._name = name;
  }

  _name;
  _books = [];

  addBook(book) {
    // check if collection has reach capacity before adding
    if (this._books.length > 20) {
      console.log(`The collection ${this._name} cannot have more than 20 books.`);
      return;
    }

    // check if this book has already been added
    if (this._books.some((bookInCollection) => book.title === bookInCollection.title)) {
      console.log(`This book has already been added to ${this._name}: ${book.title}`);
      return;
    }

    // add book to collection
    this._books.push(book);

    // sort books collection
    if (this._books.length > 1) {
      this._sortCollection();
    }
  }

  removeBook(bookTitle) {
    this._books = this._books.filter((book) => {
      return book.title !== bookTitle;
    });
  }

  getBooks() {
    return this._books;
  }

  getBooksSummary() {
    let outputObject = this._books.reduce((prev, currBook) => {
      prev.push({ title: currBook.title, popularity: currBook.popularity });
      return prev;
    }, []);
    return outputObject;
  }

  _sortCollection() {
    this._books.sort((bookA, bookB) => {
      if (bookA.popularity > bookB.popularity) {
        return -1;
      } else if (bookA.popularity < bookB.popularity) {
        return 1;
      } else {
        return 0;
      }
    });
  }
}
