import UserBookList from './user-book-list';
import data from '../content/data';

describe('user user book lists', () => {
  let myCollection = new UserBookList('new collection');

  it('create new user collection', () => {
    expect(myCollection).toEqual(expect.objectContaining({ _books: [] }));
  });

  it('add books to user collection', () => {
    expect(myCollection).toEqual(expect.objectContaining({ _books: [] }));
  });

  it('add a book', () => {
    myCollection.addBook(data[0]);
    expect(myCollection.getBooks().length).toEqual(1);
  });

  it('try to add the same book twice', () => {
    myCollection.addBook(data[0]);
    expect(myCollection.getBooks().length).toEqual(1);
  });

  it('add more books', () => {
    myCollection.addBook(data[2]);
    myCollection.addBook(data[3]);
    myCollection.addBook(data[4]);
    expect(myCollection.getBooks().length).toEqual(4);
  });

  it('remove a book', () => {
    myCollection.removeBook(data[4].title);
    expect(myCollection.getBooks().length).toEqual(3);
  });
});
